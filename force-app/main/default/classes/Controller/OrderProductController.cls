public with sharing class OrderProductController {
    
    static OrderService service = OrderService.instance;

    @AuraEnabled(cacheable=true)
    public static List<PricebookEntry> getAvailableProducts(Id orderId) {
        return service.getAvailableProducts(orderId);
    }

    @AuraEnabled(cacheable=true)
    public static List<OrderItem> getOrderProducts(Id orderId) {
        return service.getOrderProducts(orderId);
    }

    @AuraEnabled()
    public static OrderItem addProduct(OrderItem itemObject){
        return service.addProduct(itemObject);
    }

    @AuraEnabled(cacheable=true)
    public static void activate(Id orderId) {
        service.activate(orderId);
    }
}
