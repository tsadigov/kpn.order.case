public with sharing class OrderService {
    //dependencies
    public OrderItemSelector orderItems;
    public ProductSelector products;
    public OrderSelector orders;
    
    public OrderService(OrderSelector orders, OrderItemSelector orderItems, ProductSelector products){
        this.orders = orders;
        this.orderItems = orderItems;
        this.products = products;
    }

    public OrderService(){
        this(OrderSelector.instance, OrderItemSelector.instance, ProductSelector.instance);
    }

    @TestVisible
    private static OrderService s_instance;
    public  static OrderService instance{get{return s_instance = (null==s_instance) ? new OrderService():s_instance;}}


    public List<OrderItem> getOrderProducts(Id orderId) {
        return orderItems.selectByOrder(orderId);
    }

    public List<PricebookEntry> getAvailableProducts(Id orderId) {
        return products.selectAvailableProducts(orderId);
    }

    public OrderItem addProduct(OrderItem itemObject){
        
        if(null == itemObject.Quantity){
            itemObject.Quantity = 0;
        }
        itemObject.Quantity = 1 + (null == itemObject.Quantity ? 0 : itemObject.Quantity); 
        upsert itemObject;
        return itemObject;
    }

    public void activate(Id orderId){
        SapOrder.instance.activate(OrderDto.wrap(orders.getById(orderId)));
    }
}
