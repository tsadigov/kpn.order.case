public with sharing class ProductSelector {

    private static ProductSelector s_instance;
    public  static ProductSelector instance{get{return s_instance = (null==s_instance) ? new ProductSelector():s_instance;}}

    public List<PricebookEntry> selectAvailableProducts(Id orderId) {
        Id priceBookId = [SELECT priceBook2Id FROM Order WHERE Id=:orderId].priceBook2Id;
        if(null == priceBookId){
            priceBookId = [select id, name from Pricebook2 where isStandard = true limit 1].Id;
        }

        return [
            SELECT Id, Name, Product2Id, UnitPrice, ProductCode 
            FROM PricebookEntry
            WHERE Pricebook2Id=:priceBookId
            LIMIT 200
        ];
    }
}
