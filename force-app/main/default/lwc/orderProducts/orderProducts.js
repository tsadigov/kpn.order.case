import { LightningElement, wire, api, track } from 'lwc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import getData from "@salesforce/apex/OrderProductController.getOrderProducts";
import activate from "@salesforce/apex/OrderProductController.activate";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


const columns = [
    { label: 'Name', fieldName: 'ProductName__c' },
    { label: 'Unit Price', fieldName: 'UnitPrice'},
    { label: 'Quantity', fieldName: 'Quantity' },
    { label: 'Total Price', fieldName: 'TotalPrice' }
];

export default class OrderProducts extends LightningElement {
    @api 
    recordId;
    @wire(getData,{ orderId: '$recordId', refreshTime: '$refreshTime'})
    products;
    columns = columns;
    get count(){console.log(this.products);
        return this.products?.data?.length || 0;
    }

    @track
    refreshTime = Date.now();

    channel = '/data/OrderItemChangeEvent';
    subscription;

    connectedCallback(){
        this.subscribe();
    }

    handleActivate(){
        activate(this.recordId)
        .then((x)=>console.debug('OK'))
        .error(x=>{
            console.error();
            const evt = new ShowToastEvent({
                title: 'System Error',
                message: x,
                variant: 'error',
            });
            this.dispatchEvent(evt);
        });
    }

    subscribe(){

        subscribe(this.channel, -1, 
            (message)=>{
                let shouldRefresh = false;
                for(let id of message.data.payload.ChangeEventHeader.recordIds){
                    
                    if(0 <= this.products.data.findIndex((orderProduct=>orderProduct.Id == id))){
                        shouldRefresh = true;
                        break;
                    }
                }

                if(message.data.payload.ChangeEventHeader.changeType = "CREATE" && message.data.payload.OrderId == this.recordId)
                    shouldRefresh = true;

                if(shouldRefresh)
                    this.refreshTime = Date.now();
            }
        )
        .then(response => {
            this.subscription = response;
        });
    }
}